LUALATEX=lualatex
LATEX=latex
STY=graph35.sty graph35-keys.sty graph35-pixelart.sty
PDF=graph35-fr.pdf graph35-en.pdf

.PHONY: all
all: ${PDF} ${STY} logo.png

################################################################################

.PHONY: build
build: ${PDF} ${STY} README.md LICENSE.txt CHANGELOG.md
	ctanify --notds graph35.ins graph35.dtx ${PDF} ${STY} README.md LICENSE.txt CHANGELOG.md

doc/key-list.tex: graph35-key.sty
graph35-key.sty: data/keys.yaml bin/generate.keys
	./bin/generate.keys

doc/pixelart-battery.tex: graph35-pixelart.sty
doc/pixelart-function-chars.tex: graph35-pixelart.sty
doc/pixelart-function.tex: graph35-pixelart.sty
doc/pixelart-menuchar.tex: graph35-pixelart.sty
doc/pixelart-menu.tex: graph35-pixelart.sty
graph35-pixelart.sty: data/pixelart/*/*.pxl data/pixelart/*/*.tex data/pixelart/*/*.yaml
	./bin/completefunctionchars < /dev/null
	./bin/generate.pixelart


graph35.sty: graph35.ins graph35.dtx
	yes | $(LATEX) graph35.ins

graph35-fr.pdf: graph35.dtx ${STY}
	$(LUALATEX) graph35.dtx
	makeindex -s gglo.ist -o graph35.gls graph35.glo
	makeindex -s gind.ist -o graph35.ind graph35.idx
	$(LUALATEX) graph35.dtx
	$(LUALATEX) graph35.dtx
	mv graph35.pdf graph35-fr.pdf

graph35-en.pdf: graph35-en.tex ${STY}
	$(LUALATEX) graph35-en.tex
	$(LUALATEX) graph35-en.tex

.PHONY: doc
doc: graph35.dtx ${STY}
	$(LUALATEX) graph35.dtx
	mv graph35.pdf graph35-fr.pdf

################################################################################

logo.pdf: logo.tex *.sty
	pdflatex logo

logo.png: logo.pdf
	convert -density 1000 logo.pdf[0] -resize 500x500 logo.png

graph35plusE.png: logo.pdf
	convert -density 1000 logo.pdf[1] -resize 1000x1000 graph35plusE.png

################################################################################

.PHONY: clean
allclean: clean
	rm -f \
		graph35-{fr,en}pdf \
		graph35.sty

.PHONY: allclean
clean:
	rm -f \
		graph35.aux \
		graph35.glo \
		graph35.gls \
		graph35.idx \
		graph35.ilg \
		graph35.ind \
		graph35.log \
		graph35.out \
		graph35.toc

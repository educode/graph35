#!/usr/bin/env python3

# Copyright Louis Paternault 2018
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Run this file to generate the graph35-pixelart.sty file."""

import collections
import glob
import os
import textwrap

import jinja2

from ..completefunctionchars import Characters
from .. import BASEDIR, PIXELARTDIR

PXL_GLOB = os.path.join("*", "*.pxl")

STYNAME = os.path.join(
    BASEDIR,
    "graph35-pixelart.sty",
    )

DOCNAME = os.path.join(
    BASEDIR,
    "doc",
    "pixelart-{family}.tex",
    )

FUNCTIONCHAR = os.path.join(
    BASEDIR,
    "doc",
    "pixelart-function.tex",
    )

WARNING = r"""
% \iffalse
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % This file has been automatically generated. Do not manually edit it.
% % Run the `bin/generate.pixelart` script to update it.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \fi
"""

LIST = textwrap.dedent(r"""
    \begin{{itemize}}
    {ITEMS}
    \end{{itemize}}
""")

def iterate_files():
    for filename in glob.iglob(os.path.join(PIXELARTDIR, PXL_GLOB)):
        yield os.path.relpath(filename, PIXELARTDIR).split("/")

def file_dictionary():
    files = collections.defaultdict(list)
    for family, filename in iterate_files():
        files[family].append(filename)
    return files

def render_template(templatename, context):
    template = jinja2.Environment(
        block_start_string='(%',
        block_end_string='%)',
        variable_start_string='((',
        variable_end_string='))',
        comment_start_string='(#',
        comment_end_string='#)',
        loader=jinja2.FileSystemLoader(PIXELARTDIR),
        ).get_template(os.path.join(*templatename))
    context['warning'] = WARNING.strip()
    return template.render(context)

if __name__ == "__main__":
    files = file_dictionary()

    with open(STYNAME, mode="w") as sty:
        print(f"Generating '{sty.name}'…")
        sty.write(WARNING.strip())
        for family in files:
            with open(os.path.join(PIXELARTDIR, family, "macro.tex")) as templatefile:
                template = templatefile.read()
            sty.write("\n\n")
            for filename in files[family]:
                with open(os.path.join(PIXELARTDIR, family, filename)) as source:
                    sty.write(template.format(
                        name=os.path.basename(filename[:-4]),
                        content=source.read().strip(),
                        ))
    for family in files:
        if family == "function":
            continue
        with open(os.path.join(PIXELARTDIR, family, "doc.tex")) as templatefile:
            template = templatefile.read()
        with open(DOCNAME.format(family=family), mode="w") as doc:
            print(f"Generating '{doc.name}'…")
            doc.write(WARNING.strip())
            doc.write(LIST.format(ITEMS="\n".join(
                template.format(name=name[:-4])
                for name in sorted(files[family], key=str.lower)
                )))

    with Characters() as characters:
        with open(FUNCTIONCHAR, mode="w") as file:
            print(f"Generating '{file.name}'…")
            file.write(render_template(
                templatename=("function", "characters.tex"),
                context={'characters': characters.backward()},
                ))

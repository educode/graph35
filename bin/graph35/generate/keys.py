#!/usr/bin/env python3

# Copyright Louis Paternault 2018
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Run this file to generate the graph35-keys.sty file."""

import os
import textwrap

import yaml

from .. import BASEDIR

STYNAME = os.path.join(
    BASEDIR,
    "graph35-keys.sty",
    )

DOCNAME = os.path.join(
    BASEDIR,
    "doc",
    "key-list.tex",
    )

WARNING = r"""
% \iffalse
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% This file has been automatically generated. Do not manually edit it.
%% Run the `bin/generate.keys` script to update it.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \fi
"""

MACRO = r"""

\csdef{{tikzkey@{key}}}[#1]#2{{%
    \graph@tikzgenerickey{family}[%
        #1,
        name={key},
        {options}
    ]{{#2}}
}}
"""

ITEM = r"""\item \key[shift, alpha]{{{name}}} \texttt{{\textbackslash{{}}key\{{{name}\}}}}"""

LIST = textwrap.dedent(r"""
    \begin{{itemize}}
    {ITEMS}
    \end{{itemize}}
""")

def iterate_keys():
    with open(os.path.join(BASEDIR, "data", "keys.yaml")) as ffile:
        for family, keys in yaml.load(ffile.read()).items():
            for key in keys:
                yield family, key, keys[key]

if __name__ == "__main__":
    with open(STYNAME, mode="w") as sty:
        sty.write(WARNING.strip())
        for family, key, options in sorted(iterate_keys()):
            sty.write(MACRO.format(
                family=family,
                key=key,
                options=",\n        ".join("{}={{{}}}".format(key, value) for key, value in sorted(options.items())),
                ))

    with open(DOCNAME, mode="w") as doc:
        doc.write(WARNING.strip())
        doc.write(LIST.format(ITEMS="\n".join(
            ITEM.format(name=name)
            for ignored1, name, ignored2 in sorted(iterate_keys())
            )))

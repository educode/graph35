#!/usr/bin/env python3

# Copyright Louis Paternault 2018
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Fill in the `../../data/pixelart/function/characters.yaml` file."""

import argparse
import glob
import os
import re

import yaml

from .catpxl import catpxl
from . import PIXELARTDIR

RE_CHARACTERS = re.compile("^[a-zA-Z0-9_]*$")

YAMLFILE = os.path.join(PIXELARTDIR, "function", "characters.yaml")

def basename(pxlname):
    """Return the file basename (no directory, no extension)."""
    return os.path.basename(pxlname)[:-4]

def commandline_parser():
    """Return a command line parser."""
    parser = argparse.ArgumentParser(
        description='Fill missing text for function pixel arts.',
        )

    parser.add_argument(
        'files',
        nargs='*',
        help='Files to process. If empty, use all files.',
        )

    return parser

class Characters:
    """Handles dictionary of characters appearing on `function` pixelart."""

    def __init__(self):
        try:
            with open(YAMLFILE) as file:
                self._chars = {
                    key: value
                    for key, value
                    in yaml.load(file.read()).items()
                    if os.path.exists(os.path.join(PIXELARTDIR, "function", f"{key}.pxl"))
                }
        except FileNotFoundError:
            self._chars = dict()

    def __enter__(self):
        return self

    def __exit__(self, typ, value, traceback):
        if typ is not None:
            if typ not in [KeyboardInterrupt, EOFError]:
                return False

        if typ == KeyboardInterrupt:
            input("Hit Ctrl-C again to abort; <enter> to save progress.")

        with open(YAMLFILE, mode="w") as file:
            yaml.dump(
                self._chars,
                default_flow_style=False,
                stream=file,
                )

        return True

    def __iter__(self):
        yield from self._chars

    def __setitem__(self, key, value):
        self._chars[key] = value

    def __getitem__(self, key):
        return self._chars[key]

    def values(self):
        """Iterate over dictionary values."""
        yield from self._chars.values()

    def items(self):
        """Iterate over dictionary items."""
        yield from self._chars.items()

    def keys(self):
        """Iterate over dictionary keys."""
        yield from self._chars.keys()

    def backward(self):
        """Return the "backward" dictionary.

        Where values are mapped to list of keys.
        """
        backward = dict()
        for key, value in self.items():
            if value not in backward:
                backward[value] = list()
            backward[value].append(key)
        return backward

def read_chars():
    """Ask users for characters."""
    while True:
        text = input("Characters? ")
        if RE_CHARACTERS.match(text):
            return text.lower()
        print("Invalid characters.")

def main():
    """Main function."""
    options = commandline_parser().parse_args()
    if not options.files:
        options.files = glob.glob(os.path.join(PIXELARTDIR, "function", "*.pxl"))

    with Characters() as chars:
        files = set((
            filename
            for filename in set(options.files)
            if os.path.exists(filename) and basename(filename) not in chars
            ))
        while files:
            filename = files.pop()
            print("Pixel art {}…".format(basename(filename)))
            with open(filename) as file:
                catpxl(file.read())
            chars[basename(filename)] = read_chars()
            print("{} files remaining…".format(len(files)))

if __name__ == "__main__":
    main()
